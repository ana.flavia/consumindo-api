﻿using Newtonsoft.Json;
using System;
using System.Net;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            string url = "http://senacao.tk/objetos/computador";
            Computador Computador = BuscarComputador(url);

            Console.WriteLine(
                string.Format(
                    "Marca: {0} - Modelo {1} - Memoria {2}, SSD {3} Processador: {4}",
                    Computador.Marca,
                    Computador.Moledo,
                    Computador.Memoria,
                    Computador.SSD,
                    Computador.Processador
                    )
                );
            Console.ReadLine();
        }



        public static Computador BuscarComputador(string url)
        {
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);
            Console.WriteLine(content);
            Console.WriteLine("\n\n");
            Computador Computador = JsonConvert.DeserializeObject<Computador>(content);
            return Computador;

        }
    }
}