﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class Computador
    {
        public string Marca { get; set; }
        public string Moledo { get; set; }
        public int Memoria { get; set; }
        public int SSD { get; set; }
        public string Processador { get; set; }
        public List<string> Softwares { get; set; }
        public Fornecedor Fornecedor { get; set; }
    }
    class Fornecedor
    {
        public string RSocial { get; set; }
        public string Telefone { get; set; }
        public string Endereço { get; set; }

    }
}